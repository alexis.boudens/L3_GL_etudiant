
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import fibo
import sinus
import numpy as np

xs = range(10)
ys = [fibo.fiboIterative(x) for x in xs]
plt.plot(xs, ys)
plt.xlabel('x')
plt.ylabel('fiboIterative(x)')
plt.grid()
plt.savefig('plot_fibo.png')
plt.clf()


xsin = [i for i in np.arange(0,1,0.01)]
ysin = [sinus.sinus(z,2,0.25) for z in xsin]
plt.plot(xsin,ysin)
plt.xlabel('x')
plt.ylabel('sinus(x,a,b)')
plt.grid()
plt.savefig('plot_sinus.png')
plt.clf()

