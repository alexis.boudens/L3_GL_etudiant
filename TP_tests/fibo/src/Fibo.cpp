#include "Fibo.hpp"
#include <cassert>
#include <stdexcept>

int fibo(int n, int f0, int f1) {

    //Assert désactivés si compilation avec -DNDEBUG
    assert(f0 >=0);
    assert(f1 >= f0);

    //Exceptions throw
    if(n<0){throw int (-1);}

    float result = n<=0 ? f0 : fibo(n-1, f1, f1+f0);
    return result;
}

