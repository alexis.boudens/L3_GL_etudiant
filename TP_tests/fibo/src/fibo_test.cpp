#include "Fibo.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo){};
TEST(GroupFibo, test_fibo_1){
    CHECK_EQUAL(fibo(0),0);
    CHECK_EQUAL(fibo(1),1);
    CHECK_EQUAL(fibo(2),1);
    CHECK_EQUAL(fibo(3),2);
}

TEST(GroupFibo, test_fibo_2){
    try{
        fibo(-10);
        FAIL("erreur");
    }
    catch (int e) {
        CHECK_EQUAL(e, -1);
    }
}

